<?php

return [
	'dependencies' => ['core', 'backend'],
	'imports' => [
		'@sgalinski/sg-cookie-optin/' => [
			'path' => 'EXT:sg_cookie_optin/Resources/Public/JavaScript/Backend/',
		],
	],
];
